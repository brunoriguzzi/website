---
title: "Intro to Cave - Day 4"
date: 2022-03-19T16:51:02-03:00
draft: false
summary: "Chinese Garden! One of the most beautiful things I have ever seen before."
---

Day 4: Chinese Garden past first minor restriction, dive turned around 5m from first major restriction. Second short dive to deep bone room.
Dive 1: Average depth - 10.1 m ; Max depth - 15.2 m ; Dive time - 92 min
Dive 2: Average depth - 11.4 m ; Max depth - 24.0 m ; Dive time - 31 min



Unfortunately in a git accident on my part I lost all of the written content and images for this section! 
