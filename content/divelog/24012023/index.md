---
title: "Getting technical!"
date: 2023-01-24T18:30:58-05:00
summary: "An incredible dive and a great introduction to technical cave diving."
draft: false
---

{{< figure src="/escondido-top.webp" >}} 

Dive 1: Average Depth - 14.2 m ; Max depth - 27.2 m ; Dive time - 1hr 39min


Today's dive was incredible - if I had to sum it up. Awesome - as in left me in absolute awe. Not only was today a great diving day because of all the new skills learnt and used, but also because of the place we visited. Cenote Escondido, Hostage Hole (both deep lines), is probably my favorite cenote so far. 

Above ground the Cenote is already an unique one. I don't really know what attracted me to it, but it somehow felt very different from the other cenotes I have been to. The fauna is quite peculiar with short palm trees covering all the area around the cenote. The cenote itself was a little reminiscent of a small lake, maybe because of how long it was. It seemed less pool-like than others around. 

For this dive, we were using three cilinders, and our stage bottle was filled with Nitrox 32 instead of air. This richer air would help reducing the little deco time that we would eventually have when leaving the cave - not only leaving, but I will get to that soon enough. We rigged our gear and analyzed our gasses. While this is always important, it becomes even more so when the gas you breathe can artificially create ceilings for you during a dive. Hence, you must be absolutely certain that what you are breathing and calculating with is exactly what you expect it to be. The same apply for the air bottles, since your cilinder filler was doing Nitrox on some bottles, maybe they got confused and did them all nitrox, so you must also check the air ones to make sure they are what you expect them to be. 

The plan was to go towards Hostage Hole, and dive both deep lines present. Cenote Escondido, in all its wonder and awe, has honestly really shitty names for its tunnels...Tunnel A, Tunnel B, and Tunnel C. How terrible is that? Hence it is a lot cooler to define where you are going by the room/area you are going to here, in this case it was called Hostage Hole. (Maybe also questionable depending on who you ask?) 

The entrance to the cave already starts out being unique. The cenote looks like a normal lake, more or less, but to the side by the wall there is a sliver of darkness you can see. This is no wider than me standing up on it - maybe around a meter and a half of so? This sliver gives way to a bigger room inside the ground - but you must first squeeze (not literally now) your way to that. This ends up being the gist of this entire dive, really narrow spaces that lead to GIANT rooms. These other rooms are truly wonderous, I feel like an entire A380 could fit in there, wingspan and all. The way into the cave I was breathing my Nitrox mix. The idea was to use it as much as possible but leaving a little for the deco that would come later on. 

The first tie-off to the cave line was the farthest we have done yet, I felt my primary reel get really light as we made our way into the cave and finally did the tie-off at the cave line. After that, we proceed to go through a series of big rooms followed by tight passages. This all the way down until you reach the proper hostage hole. This massive room that just makes you feel small and inconsequent compared to its glory. Here, for the first time, I felt a little...scared? Maybe because the size of the room just made me feel so small that I became freightened by the fact that I was so deep inside a cave without a way to get out fast. It was my first experience of fear (again, not sure this is exactly the right word?) inside a cave. I closed my eyes for a second and took a deep breath out of my regulator. It was working perfectly. I took another one. It was still working perfectly. I then checked my air on all three of my tanks, they were all within very safe limits. Ok, time for another deep breath and to keep going. I instantly thought about my sidemount instructor, José Mario Ventura - one of the most badass cave divers out there, even having his name in a couple cave diving manuals. The one thing he told me when I said I was starting the cave diving adventure - always remember to breathe, focus your mind, and work towards any problems. Don't let them control you, but control them instead. These words finally sank in today, and after checking all that I had to check I was good to go again. Fear is normal, I mean, what the hell - I am deep inside a cave underwater. What matters is how you deal with that. 

^ For now I will end this post like this, it does not seem right to write more now. Maybe later i'll come back to add something else. 
