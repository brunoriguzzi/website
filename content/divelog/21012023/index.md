---
title: "Dropped a Primary Light"
date: 2023-01-21T19:42:07-03:00
summary: "Pet Cemetery Cenote; Dark Side of the Moon line."
draft: false
---

{{< figure src="/cave.webp" >}}
 

Dive 1: Average Depth - 7.8 m ; Max depth - 11.1 m ; Dive time - 1hr 41min


It has been a while since I have been in the water; especially in a cave. Although showers as well, I hate those :) . I am back in Mexico for my Deco and Advanced Nitrox courses. It will be a jam-packed 7 days of diving, and to begin we had to get as much practice of practice concepts as possible while also doing a long and enjoyable dive. Luis took me today to Pet Cemetery Cenote, one I haven't had the pleasure of visiting yet. The Cenote has one of the nicest infrastructures I have seen in Mexico so far, and the above-ground part of it is idilic. Truly feels like I am in paradise. Apparently the entrance fee is a little steep at around 80 USD, which deters many foreigners that are only interested in coming for a short snorkel experience or to check the "Cancun To-Do list" and do a short Overhang dive. This is greate because there aren't many people there, and those present are invested in what they are doing. 

{{< figure src="/sign.webp" >}}

I have some new gear that I was "baptising" for the first time in a cave - especially a new prescription mask - which gave me a little of a headache. The mask is brand new and I forgot to do all the ritual preparations to get the fog-causing chemicals out of the lenses. This was a big big mistake, leaving me having to flood my mask to clear the fog every two minutes or so. I strongly considered switching to my backup mask, but that has quite an outdated prescription so it was a gamble. 

The dive was spectacular. Never have I seen so many stalactites and stalagmites together and covering such an area in the cave. This was an almost two hour dive, and almost all of it was just cave feature after cave feature. I wish I could see better - damn fogging glasses. I wonder if the prescription lens mounting process has something to do with it? 

Anyways, to the title of the post. The dive was done and we had exited the water and begun packing our gear to leave. This is when I realized - my backup primary light was gone. Shit. All that was left was a double-eye bolt snap. Lesson learned, never ever tie important gear simply with loops around a double eyed bolt snap. It is much more prudent to use a swivel eye bolt snap and securly attach the gear to it with a good knot. Thankfully, another dive team going through the same path as us right after, and friends of Luis, found the light somewhere in the cave. As I am writing this I have no idea where it was, but I am curious to find out tomorrow where they were able to spot it. 

I guess this brings me to another interesting fact that I had no idea. It is hard to lose things in caves. The dive community here in Mexico is quite tight, and if you drop something and cannot go back for it that same day, there is a big chance that someone you know will grab it for ỳou if they are following the same line. As I was stressing about loosing an over 1000 USD light, Luis was very calm knowing the next team would probably find it once they pass by it. This is of course if it does not fall into some crevice or such. But, at least today I will sleep better knowing I won't have to buy another primary light. 

After the dive, I drove to Mamalonas Burger near by Airbnb in Puerto Aventures. I must say, this is one of the best burger joints I have eaten in. Their fries are incredible and the size and taste of the burgers are unparalleled. 

{{< figure src="/burger.webp" >}}
