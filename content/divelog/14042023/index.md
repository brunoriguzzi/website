---
title: "Staging at Callimba"
date: 2023-04-14T18:07:44-05:00
summary: "Callimba Cenote and stage practice"
draft: false
---

{{< figure src="/tanks.webp" >}}

Dive 1: Average Depth - 10.1 m ; Max depth - 13.6 m ; Dive time - 1 hr 48 min

Callimba is one of my favorite cenotes so far. Its formations are beautiful and its layout is incredible. The cave goes from a curtain of stalagtites andstalagmites to tight smooth restrictions in a matter of a few meters. There are vertical passageways that we must climb into like spiderman, others where you must dive into like Mario entering a pipe. There are tight slips like those in videogames that you have to squeeze through at an angle, and others where you almost can cross sideways because of how wide they are. Callimba is so incredibly diverse. Today we practiced with stages. The diveplan was to enter on our stage and breathe no more than 1/2 of it before we reached a jump. Here, we would setup a circuit and leave our stage behind. Following the circuit on our sidemount only cylinders, we stuck to the normal 1/3 ratio.  
