---
title: "Taak Bi Ha to El Pit"
date: 2023-04-16T15:34:44-05:00
summary: "Following the footsteps of the explorers who discovered El Pit"
draft: false
---

{{< figure src="/elpit.webp" >}}

Dive 1: Average Depth - 9.3 m ; Max depth - 16.8 m ; Dive time - 2 hr 24 min

This was an unique dive. We followed the footsteps of the explorers who first discovered El Pit. Diving from a Cenote called Taak Bi Ha, the same that I visited for my birthday on the beginning of this year, you can reach a wonderous cenote called El Pit. El Pit, as the name suggests, is a super deep cenote in the Dos Ojos system. El Pit has a couple different rooms at around 80m and even deeper! 

The dive began at Taak Bi Ha, an already beautiful cenote unlike many in the area. Taak Bi Ha has a small entrance staircase which leads to a big underground room with bright blue water leading deeper into the veins of the earth. Cylinders and other gear like scooters are lowered through the ceiling on a winch system. (Honestly kind of nice not having to carry all your gear!) We had two options, to dive to a zone of Taak Bi Ha called LSD, or instead to head through some small caves and into El Pit - around 70 min from Taak Bi Ha. Most of the dive is done with a scooter, except for the last 20 minutes ish where it becomes quite small and hence it is easier to leave them behind, and also our single stage, and head into the rest of the cave simply with our sidemount cylinders. On the way there is another cenote called Dos Ojitos. A little twin brother to Taak Bi Ha, Dos Ojitos was my first time exiting a dive into a cave with a breathable atmosphere. Dos Ojitos is a cenote with an entrance quite deep inside a cave, and so there is no light whatsoever from outside. There is, though, fresh breathable air! The experience of exiting a underground river into a dark cave is something taken right out of a Batman comic! I was ecstatic! But, I digress. The showstopper for this dive for sure had to be El Pit. Similarly to Blue Abyss, we exit from a small tunnel into a void. But, unlike Blue Abyss where all you see is darkness, El Pit has a beam of light far into the distance. A beam that penetrates all the way from the surface down to a milky cloud-like layer of Hydrogen Sulfide. A Hydrogen Sulfide layer forms from organic matter decomposition in the absence of Oxygen. And what is most cool of all, is that it sits right at the Halocline boundry. It is denser than fresh water hence it sinks, but it is less dense than salt water hence it sits atop of it. Truly something out of this world. 

Oh, to explain the summary of this post. This path we took, from Taak Bi Ha to El Pit was the path that the first explorers to discover it took! Imagine being blessed with such a discovery! El Pit was previously unknown to any in the area, and it was discovered by divers exploring the system from Taak Bi Ha cenote. Now, there is an access from the top! Our exit into El Pit was at aroud 13m, but at the very top we could spot some single-cylinder divers exploring the top of the cenote. Must have been a cool view for them as well! 

El Pit is a dive we have to go back - but, it requires Trimix, something I am yet not certified for! I cannot wait to come back here and reach through the layers of Hydrogen Sulfide into the deep rooms that this Cenote has to offer. 
