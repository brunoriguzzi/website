---
title: "Birthday Dive"
date: 2023-01-23T18:30:54-05:00
summary: "Taak Bi Ha; Cave and Cavern Dives"
draft: false
---

{{< figure src="/bday.webp" >}} 

Dive 1: Average Depth - 4.3 m ; Max depth - 7.2 m ; Dive time - 46 min
Dive 1: Average Depth - 4.7 m ; Max depth - 7.6 m ; Dive time - 34 min

Today was my birthday! Our dive today was a mix of Cavern line and Cave line. My dad and another diver joined us, hence today we had a dive team of 4 people! The first dive we did our way around the cavern line, setting up a jump from the cavern line to a cave line, but then coming back and returning through the cavern line. This was to create a circuit that we would then make use of during the second dive during the cave portion. 

Oh, the cenote itself is an unique one, as the entrance to it is really inside a cave itself. More hidden then escondido which I dive tomorrow, the 24th haha. The cenote is beautiful, both for divers and also snorklers. The water is pristine and the setup is incredibly. The entrance to it is a tiny hole on the floor with a steep and rather sketchy looking staircase. As soon as I saw that, I was like...there is no way I can take cilinders down that thing. Thankfully, Taak Bi Ha has a special delivery system. Another hole on the ceiling. This is like a reverse wishing well tbh. Divers below wish for a cilinder to appear and somehere from the heavens through a small hole in the ceiling one slowly gets lowered down. Felt like I was receiveing the Commandments - that's how epic it was. 

Overall the dive was a very pleasant one. It was great to get some cave time in and also practice my second ever circuit. There is quite some choreography and techniques involved when traversing one. Once we reach the jump marker setup during dive 1, we are technically exiting the cave system now. I must confirm with my partners that we are actually going to leave the cave through the jump. For this we do the common gun pointing finger sign (not really sure how to call it) towards the exit marked by the jump. All other partners must agree that our exit is in fact "forward," and not back through the line we just came through like we normally do. Once this is agreed, the leader has to stay behind to retrieve the reel and markers and continue in the back of the bunch, as we are now exiting the cave. Once the jump is crossed, we must agree on which side to travel, and this is clearly marked again by an arrow or REM placed during the first dive. 

Overall circuits and really interesting navigation tools to use, and bring some uniqueness to a dive. 

After diving, I drove an hour ish to Tulum beach area to have dinner with family at the Casa Malca hotel. Beautiful place. 
