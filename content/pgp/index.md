---
title: "Contact Me"
---

Preferably with PGP/GnuPG 

Key ID (4096-bit): Bruno Riguzzi <bruno@riguzzi.io> - 0x4904D7BD

Fingerprint: A093 D421 C74B 25B9 844B  1D26 2644 26FD 4904 D7BD

Public key: {{< dir path="/content/pgp/pubkey" pathURL="/pgp/pubkey" >}}

