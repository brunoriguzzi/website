---
title: "eJPT"
date: 2022-02-27T21:39:43-03:00
draft: false
summary: "Some contemplations and considerations after completing the eJPT (junior pen-tester) exam"
---

Yesterday I passed my eJPT exam, a practical penetration testing exam. Here are some things I learned.

First, the importance of being patient, and doing good enumeration. Apart from the BlackBoxes I did to prepare for this exam, this was one of three "actual" penetration tests I have conducted. And arguably the stakes are higher here because there is a certification on the line. So, I was a little stressed and unsure of myself for the first hour of so of the exam. My enumeration skills were weak, and I wasted time confused between two machines.

Second, letters of engagement are crucial. I did not read mine with care, and missed the point where I had to route to other networks. I was fully expecting to reach other networks, but I would've guessed it would look more like the BlackBoxes, where I had to pivot. Anyways, in the first hour or so where I was dreading this, thinking I was not prepared, I decided to take a break, breathe, pat the dog, and take it a step at a time. I started by re-reading the engagement letter which told me about the different networks I could find from the pcap file. From then on the exam was a breeze, in the sense where I had fun doing it and was not stressing over my skills.

Third, I realized I need a better method of staying organized. Between saving results, saving different attempts, and having multiple instances of things running at the same time I was a little lost. I had Hydra and John running to crack passwords (I setup vms to do this on a server I have in my homelab to not stress my computer with cracking), I had nmap running a more detailed vuln scan of a machine, I had enum4linux enumerating a Windows machine, and I had metasploit open on another tab. This plus the miriad of browser tabs where I was looking things up. My organization failed me, and I confused results from two Windows machines, and then started cracking my head when a CVE I found was not working on a machine where NMap told me it would work.

Fourth, doing this sort of penetration test is a humbling experience. It showed me how much I still have to learn. Hydra and john work wonders on a machine where they are meant to work wonders, where it is setup for it to be easy. In reality, I doubt many machines will take kindly to the sort of abuse Hydra throws at it, and most should lock me out instantly. This I still have no idea how to bypass. But again, this is a Junior cert. I am not expected to know how to bypass defences yet, but I am excited to find ways to do so. I guess what is most important now is that I realize how far I have to go. I try to always be cautious about the dunning-krueger effect.
